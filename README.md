# CS 126 FinalProject
## Pong
### Disclaimer
This project uses socket code and has only been run on a linux platform. It might run on OSX out of the box and It will definitely not run on windows unless you have Cygwin or want to port `pong_client.cpp` to winsock.
### Installation.
Edit the Makefile to put the path to your openFrameworks directory.
Run `# make` and do what it tells you to do.
If you wish to make the server, you need to remove the `.t` of `src/server/pong_server.cpp.t` and run `sudo make server`.
If you need to rebuild the client after making the server, make sure to replace the `.t`.
Don't blame me for the server debacle, the openFrameworks Makefile will throw a fit at the main in `pong_server.cpp` if you run `# make` with it.

### Connecting to a server.
Enter the server address (this can be an IP). Enter the port it's on. Enter your connection password.

### Gameplay
If you've managed to build the project yourself, the menu should be hopefully self-explanitory. Press the buttons to change the settings.

In local multiplayer, `a` and `d` are fast movement, while `q` and `e` are slow movement for player two.
Player one uses the mouse.

In network multiplayer, you use can chat with the keyboard keys and you use the mouse to move.
Unfortunately, chat is only lowercase.

### (P.S.)
If I attach Tux the Linux Penguin to this project, it's gonna be 80% there to ye standard wierd Linux game.
