# Project Proposal
* Network based pong game.
* Play on LAN or across the world (provided you're not behind a NAT).
* There will be a chat feature, so you can be rude to your opponents, like you ought to in an online game.
* External Libraries
  * openFrameworks Sound
  * openFrameworks 3D

