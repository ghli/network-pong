# Attempt to load a config.make file.
# If none is found, project defaults in config.project.make will be used.

ifneq ($(wildcard config.make),)
	include config.make
endif

# make sure the the OF_ROOT location is defined
ifndef OF_ROOT
	OF_ROOT=$(realpath ../openFrameworks)
endif

# call the project makefile!
include $(OF_ROOT)/libs/openFrameworksCompiled/project/makefileCommon/compile.project.mk

.PHONY: server
server : src/server/pong_server.cpp
	g++ -o3 -o bin/server $^ -Isrc -pthread -lpthread
