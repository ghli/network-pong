#ifndef PONG_PADDLE
#define PONG_PADDLE
#include "ball.h"

// PongGame includes paddle, so we'll foreward declare.
class PongGame;

class Paddle : public Object {
    friend class PongGame;
    friend class Ball;
public:
    // Constructor (x coord, y coord, width, height).
    Paddle(double, double, double, double);

    // Handles a collision with a ball.
    // Returns a true if ball has gotten through, else false.
    bool handle_collision(Ball&);

    // See Object documentation (this is a overwritten function).
    double get_top_left_x();
    double get_top_left_y();

    // Getters.
    double get_w();
    double get_h();
    int get_score();

    // Setters.
    void set_score(int);
private:
    // Dimensions.
    double w_;
    double h_;

    int score_;


};
#endif
