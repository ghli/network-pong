#include "pong_game.h"
#include <iostream>
#include "constants.h"
#include <string.h>

PongGame::PongGame() : one_(kDefaultX,
                            kDefaultH,
                            kDefaultPaddleWidth,
                            kDefaultThickness),
                       two_(kDefaultX,
                            0,
                            kDefaultPaddleWidth,
                            kDefaultThickness),
                       ball_(kDefaultW/2,
                             kDefaultH/2,
                             kDefaultThickness),
                       w_(kDefaultW),
                       h_(kDefaultH) {}

void PongGame::set_w(double w) {
    w_ = w;
} 
void PongGame::set_h(double h) {
    h_ = h;
}

void PongGame::set_second(bool second) {
    second_engine_ = second;
}

double PongGame::get_w() {
    return w_;
}

double PongGame::get_h() {
    return h_;
}

void PongGame::update() {
    save_velocity();

    // Move the objects.
    one_.move();
    two_.move();
    ball_.move();

    // Bounds Checking.
    if (one_.x_ > w_)
        one_.x_ = w_;
    if (one_.x_ < 0)
        one_.x_ = 0;
    if (two_.x_ > w_)
        two_.x_ = w_;
    if (two_.x_ < 0)
        two_.x_ = 0;
}

void PongGame::collision_detection() {
    // Side wall collision detection.
    if (ball_.x_ - ball_.r_/2 + ball_.v_x_ < 0
        || ball_.x_ + ball_.r_/2 + ball_.v_x_ > w_) {
        ball_.v_x_ *= -1;
    }

    // Offload collision detection to paddles.
    // This way, special paddles are easier.
    // split the ball detection into the two paddles' sections to preserve,
    // Processing power.
    if (abs(ball_.y_ - two_.y_) < .1*h_) {
        if (two_.handle_collision(ball_)) {
            one_.score_++;
            reset();
            shoot_ball();
        }
    } else if (abs(ball_.y_ - one_.y_) < .1*h_) {
        if (one_.handle_collision(ball_)) {
            two_.score_++;
            reset();
            shoot_ball();
        }
    }
}

void PongGame::move_one(double x, double y) {
    one_.move(x,y);
}

void PongGame::move_two(double x, double y) {
    two_.move(x,y);
}

void PongGame::move_ball(double x, double y) {
    ball_.move(x,y);
}

void PongGame::set_v_one(double vx, double vy) {
    one_.v_x_ = vx;
    // Paddles don't travel through y-space.
}

void PongGame::set_v_two(double vx, double vy) {
    two_.v_x_ = vx;
    // Paddles don't travel through y-space.
}

void PongGame::set_v_ball(double vx, double vy) {
    ball_.v_x_ = vx;
    ball_.v_y_ = vy;
}

Paddle PongGame::get_one() {
    return one_;
}

Paddle PongGame::get_two() {
    return two_;
}

Ball PongGame::get_ball() {
    return ball_;
}

void PongGame::reset() {
    // Moves the objects to default position.
    one_.move(w_/2, h_);
    two_.move(w_/2, 0);
    ball_.move(w_/2, h_/2);
    // Stops the ball.
    ball_.stop();
    reset_ = true;
}

void PongGame::shoot_ball() {
    // Gives ball random direction.
    ball_.v_x_ = (double)(1 + rand()%10);
    ball_.v_y_ = (double)(3 + rand()%10);
}

GameInfo PongGame::serialize() {
    // Create the struct.
    GameInfo info;

    memset(info.control_msg,0,10);

    // Fill up the info.
    // We need to rotate everything,
    // So we can have both clients play on the bottom.
    if (!second_engine_) {
        info.p1_score = one_.score_;
        info.p2_score = two_.score_;
        info.p1_x = one_.x_;
        info.p1_y = one_.y_;
        info.p2_x = two_.x_;
        info.p2_y = two_.y_;
        info.ball_x = ball_.x_;
        info.ball_y = ball_.y_;
        info.ball_vx = ball_.v_x_;
        info.ball_vy = ball_.v_y_;
    } else {
        info.p1_score = two_.score_;
        info.p2_score = one_.score_;
        info.p1_x = w_ - two_.x_;
        info.p1_y = h_ - two_.y_;
        info.p2_x = w_ - one_.x_;
        info.p2_y = h_ - one_.y_;
        info.ball_x = w_ - ball_.x_;
        info.ball_y = h_ - ball_.y_;
        info.ball_vx = -ball_.v_x_;
        info.ball_vy = -ball_.v_y_;
    }

    return info;
}

void PongGame::save_velocity() {
    old_vx_ = ball_.v_x_;
    old_vy_ = ball_.v_y_;
}

void PongGame::deserialize(GameInfo& info) {
    save_velocity();

    // Just copy the data over, b0ss.
    // We have to rotate back if we're the second player.
    if (!second_engine_) {
        one_.score_ = info.p1_score;
        two_.score_ = info.p2_score;
        one_.x_ = info.p1_x;
        two_.x_ = info.p2_x;
        one_.y_ = info.p1_y;
        two_.y_ = info.p2_y;
        ball_.x_ = info.ball_x;
        ball_.y_ = info.ball_y;
        ball_.v_x_ = info.ball_vx;
        ball_.v_y_ = info.ball_vy;
    } else {
        one_.score_ = info.p2_score;
        two_.score_ = info.p1_score;
        one_.x_ = w_ - info.p2_x;
        two_.x_ = w_ - info.p1_x;
        one_.y_ = h_ - info.p2_y;
        two_.y_ = h_ - info.p1_y;
        ball_.x_ = w_ - info.ball_x;
        ball_.y_ = h_ - info.ball_y;
        ball_.v_x_ = -info.ball_vx;
        ball_.v_y_ = -info.ball_vy;
    }
}

bool PongGame::bounced() {
    // Don't play a sound if the ball is not near the edges.
    if (abs(ball_.x_) > kDefaultPaddleWidth
        && abs(ball_.x_ - w_) > kDefaultPaddleWidth
        && abs(ball_.y_) > kDefaultPaddleWidth
        && abs(ball_.y_ - h_) > kDefaultPaddleWidth)
        return false;

    // If we've changed V, it's a bounce.
    if (old_vx_ != ball_.v_x_
        || old_vy_ != ball_.v_y_)
        return true;
    return false;
}
