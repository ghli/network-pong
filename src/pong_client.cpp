#include "pong_game.h"
#include "pong_client.h"
#include <iostream>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include "game_info.h"
#include "constants.h"
#include <string>

std::string PongClient::init(std::string url,
                             std::string port,
                             std::string password) {
    opponent_quit_ = false;
    // structs to help us lookup network info.
    struct addrinfo hints;
    struct addrinfo *lookup_results;

    // Zero the hints, just in case.
    memset(&hints, 0, sizeof hints);
    // We don't care if it's ipv4 or ipv6.
    hints.ai_family = AF_UNSPEC;
    // We want a TCP connection.
    hints.ai_socktype = SOCK_STREAM;

    // getaddrinfo returns 0 on failure.
    if (getaddrinfo(url.c_str(), port.c_str(), &hints, &lookup_results)) {
        return "Network lookup failed.";
    }

    // Get the socket based on the lookup results.
    // Family is basically ipv6 or ipv4 (there's some exotic ones too).
    // Socktype is TCP stream/UDP datagram (and some exotic ones we don't care about).
    // Protocol is the communications protocol, which really doesn't matter for us.
    connection_fd_ = socket(lookup_results->ai_family,
                            lookup_results->ai_socktype,
                            lookup_results->ai_protocol);

    // File Descriptors can't be less than zero, so if socket() returned that,
    // Something went wrong.
    if (connection_fd_ < 0) {
        return "Failed to acquire socket.";
    }

    // Connect returns 0 on error.
    if(connect(connection_fd_,
               lookup_results->ai_addr,
               lookup_results->ai_addrlen)) {
        return "Failed to establish connection.";
    }

    // Free the memory or lookup results.
    freeaddrinfo(lookup_results);

    // Send our password.
    send(connection_fd_, password.c_str(), kPasswordLen, 0);

    // Recieve our first communication from the server.
    recv(connection_fd_, &info_, sizeof(info_), MSG_WAITALL);

    // If we're the first to connect, we get 1 as the control msg, else 2.
    if (strcmp(info_.control_msg, "1") == 0) {
        local_simulation_ = true;
        set_second(false);
        std::cout << "ONE!" << std::endl;
    } else {
        local_simulation_ = false;
        set_second(true);
        std::cout << "TWO!" << std::endl;
    }

    return "Success.";
}

bool PongClient::early_exit() {
    return opponent_quit_;
}

void PongClient::send_data() {
    // "Serialize" our data.
    info_ = serialize();

    // If we have a message to send the opponent, include it in the package.
    if (outgoing_msg_.size() > 0) {
        // Copy the message over.
        strncpy(info_.chat_msg,
                outgoing_msg_.front().c_str(),
                sizeof(info_.chat_msg));
        outgoing_msg_.pop_front();
        // Since strncpy does not implicitly append a \0 if source > dest size,
        // We do it at the very end just in case.
        info_.chat_msg[sizeof(info_.chat_msg) - 1] = '\0';
    } else {
        // We zero the chat message, because c++ doesn't do it for us,
        // Because that's how we tell if we've gotten a message or not.
        memset(info_.chat_msg, 0, sizeof(info_.chat_msg));
    }
    // Send that data.
    send(connection_fd_, &info_, sizeof(info_), 0);
}

void PongClient::recieve_data() {
    // Recieve communications from the server.
    recv(connection_fd_, &info_, sizeof(info_), MSG_WAITALL);

    opponent_quit_ = false;
    // Check if the opponent borked.
    if (strcmp(info_.control_msg, "D") == 0) {
        opponent_quit_ = true;
    }

    // If the opponent sent us a message.
    if (strlen(info_.chat_msg) > 0) {
        // Add it to the list.
        opponent_msg_.push_back(std::string(info_.chat_msg));
    }

    // Deserialize the info.
    deserialize(info_);
}

void PongClient::update() {
    // If we're not doing network stuff.
    if (local_game_) {
        ((PongGame*)this)->update();
        ((PongGame*)this)->collision_detection();
        return;
    }

    // Our system switches off what system does the actual simulations.
    // They alternate every update.
    // Unacceptable for a professional game, but this is pong.
    if (local_simulation_) {
        // Update ourself with the base PongGame update.
        ((PongGame*)this)->update();
        ((PongGame*)this)->collision_detection();
        send_data();
    } else {
        recieve_data();
    }
    // Switch.
    local_simulation_ = !local_simulation_;
}

bool PongClient::is_local() {
    return local_game_;
}

void PongClient::set_local(bool b) {
    local_game_ = b;
}

std::deque<std::string>& PongClient::get_msg() {
    return opponent_msg_;
}

void PongClient::send_msg(std::string msg) {
    outgoing_msg_.push_back(msg);
}
