#include "ofApp.h"
#include "constants.h"
#include <ctime>

void Ponger::set_params(double x, double y) {
    // Set the 
    pg_.set_w(x);
    pg_.set_h(y);
    pg_.reset();
}

void Ponger::mode_toggle_action() {
    pg_.set_local(!pg_.is_local());
}

void Ponger::address_entry_action() {
    address_ = ofSystemTextBoxDialog(kAddressMsg, kBlankStr);
}

void Ponger::port_entry_action() {
    port_ = ofSystemTextBoxDialog(kPortMsg, kBlankStr);
}

void Ponger::password_entry_action() {
    password_ = ofSystemTextBoxDialog(kPasswordMsg, kBlankStr);
}
void Ponger::game_start_action() {
    std::string msg;
    if (!pg_.is_local())
        msg = pg_.init(address_, port_, password_);
    if (!pg_.is_local() && msg != "Success.") {
        ofSystemAlertDialog(msg);
        std::cout << "help" << std::exit;
        std::exit(0);
    }
    pg_.shoot_ball();
    started_ = true;
}

void Ponger::setup() {
    // Initialize resources.
    ofSetWindowTitle("Pong");
    srand(static_cast<unsigned>(time(0)));
    score_text_.load("Batang.ttf", 160, true, true, true);
    chat_text_.load("Batang.ttf", 24, true, true, true);
    bouncy_.load("bouncy.mp3"); 

    // Setting default parameters.
    address_ = kDefaultAddr;
    port_ = kDefaultPort;
    password_ = kDefaultPass;
    pg_.set_local(true);

    // Setting buttons.
    mode_toggle_.addListener(this, &Ponger::mode_toggle_action);
    address_entry_.addListener(this, &Ponger::address_entry_action);
    port_entry_.addListener(this, &Ponger::port_entry_action);
    password_entry_.addListener(this, &Ponger::password_entry_action);
    start_button_.addListener(this, &Ponger::game_start_action);

    // Adding buttons and labels to menu.
    menu_.setup();
    menu_.add(mode_toggle_.setup("Change mode"));
    menu_.add(mode_label_.setup("Mode:",""));
    menu_.add(address_entry_.setup("Change address"));
    menu_.add(address_label_.setup("Address:", address_));
    menu_.add(port_entry_.setup("Change port"));
    menu_.add(port_label_.setup("Port:", port_));
    menu_.add(password_entry_.setup("Change Password"));
    menu_.add(password_label_.setup("Password:", password_));
    menu_.add(start_button_.setup("Start"));
}

void Ponger::update() {
    // If we haven't started, all we need to do is to update the menu.
    if (!started_) {
        mode_label_ = ofToString(pg_.is_local() ? "Local" : "Network");
        address_label_ = address_;
        port_label_ = port_;
        password_label_ = password_;
        return;
    }

    // Update game state.
    pg_.update();
    expire_messages();

    // If our opponent quit, quit as well.
    if (pg_.early_exit()) {
        ofSystemAlertDialog(kOpponentQuitMsg);
        std::exit(0);
    }

    // Play bouncy sound.
    if(pg_.bounced())
        bouncy_.play();
}

void Ponger::draw_game_objects(double xoffset, double yoffset) {
    // Store the top-down view.
    ofPushMatrix();
    // Good looking rotation.
    ofRotateXDeg(kGraphicalRotation);

    Paddle one = pg_.get_one();
    Paddle two = pg_.get_two();
    Ball ball = pg_.get_ball();

    // Draw the paddles and ball.
    ofDrawBox(xoffset + one.get_x(),
              yoffset + one.get_y() + one.get_h()/2,
              0,
              one.get_w(),
              one.get_h(),
              kDefaultThickness);
    ofDrawBox(xoffset + two.get_x(),
              yoffset + two.get_y() - two.get_h()/2,
              0,
              two.get_w(),
              two.get_h(),
              kDefaultThickness);
    ofDrawSphere(xoffset + ball.get_x(),
              yoffset + ball.get_y() + ball.get_h()/2,
              0,
              ball.get_w());

    // Draw the ball's path.
    ofDrawLine(xoffset + ball.get_x(),
               yoffset + ball.get_y(),
               0,
               xoffset + ball.get_x() + kArrowLen * ball.get_vx(),
               yoffset + ball.get_y() + kArrowLen * ball.get_vy(),
               0);
    
    // Draw the wireframe of the paddles and ball.
    ofNoFill();
    ofSetColor(0,0,0);
    ofDrawBox(xoffset + one.get_x(),
              yoffset + one.get_y() + one.get_h()/2,
              0,
              one.get_w(),
              one.get_h(),
              kDefaultThickness);
    ofDrawBox(xoffset + two.get_x(),
              yoffset + two.get_y() - two.get_h()/2,
              0,
              two.get_w(),
              two.get_h(),
              kDefaultThickness);

    // Draw the boundary of the "court".
    ofSetColor(255, 255, 255);
    ofDrawRectangle(xoffset, yoffset, pg_.get_w(), pg_.get_h());

    // Restore vanillia view.
    ofPopMatrix();

}

void Ponger::draw() {
    ofFill();

    if(!started_)
        menu_.draw();

    // Offset to center an object within another object.
    double xoffset = (ofGetWindowWidth() - pg_.get_w())/2;
    double yoffset = (ofGetWindowHeight() - pg_.get_h())/2;

    draw_game_objects(xoffset, yoffset);

    ofFill();
    ofSetColor(0, 0, 0);
    // Draw the chat message stuff.
    chat_text_.drawString(msg_buf_, xoffset, yoffset + 200);
    for (int i = 0; i < pg_.get_msg().size(); ++i) {
        std::string msg = pg_.get_msg()[i];
        chat_text_.drawString(msg,
                xoffset,
                yoffset + i * chat_text_.stringHeight(msg));
    }

    ofNoFill();
    ofSetColor(255, 255, 255);
    char msg[20];
    // Draw the score.
    snprintf(msg,
             20,
             "%d\n%d",
             pg_.get_two().get_score(),
             pg_.get_one().get_score());
    score_text_.drawStringAsShapes(string(msg),
            (ofGetWindowWidth() - score_text_.stringWidth(string(msg)))/2,
            (ofGetWindowHeight() - score_text_.stringHeight(string(msg)))/2);
}

void Ponger::keyPressed(int key) {
    // If we haven't started the game, disallow movement.
    if(!started_)
        return;
    // If the game is local, keys move paddle two.
    if (pg_.is_local()) {
        if (key == 'a') {
            pg_.set_v_two(-pg_.get_w()/kSpeedScaling, 0);
        }
        if (key == 'q') {
            pg_.set_v_two(-kSlowSpeed, 0);
        }
        if (key == 'd') {
            pg_.set_v_two(pg_.get_w()/kSpeedScaling, 0);
        }
        if (key == 'e') {
            pg_.set_v_two(kSlowSpeed, 0);
        }
    // If it's not local, keys type messages.
    } else {
        // If the key is printable, add it to the msg buffer.
        if (isprint(key)) {
            msg_buf_ += (char) key;
        // Enter sends the message.
        } else if (key == OF_KEY_RETURN) {
            if (msg_buf_.size() > 0) {
                pg_.send_msg(msg_buf_);
                msg_buf_ = "";
            }
        } else if (key == OF_KEY_BACKSPACE) {
            if (msg_buf_.size() > 0)
                // Erase the last char.
                msg_buf_.erase(msg_buf_.size() - 1, 1);
        }
    }
    
}

void Ponger::mouseMoved(int x, int y) {
    // Only move if we've started the game.
    if (started_) {
        double x_scaled = pg_.get_w() * x / ofGetWindowWidth();
        pg_.move_one(x_scaled, pg_.get_one().get_y());
    }
}

void Ponger::keyReleased(int key) {
    // Behaviour is same for both lower case and caps.
    // Only set it if we're having a local game.
    if (pg_.is_local())
        pg_.set_v_two(0, 0);
}

void Ponger::expire_messages() {
    // Timer should only tick if we have messages.
    if (pg_.get_msg().size() == 0)
        last_msg_clear_ = time(0);

    if (time(0) - last_msg_clear_ > kMsgExpirationTime) {
        last_msg_clear_ = time(0);
        if (pg_.get_msg().size() > 0) {
            pg_.get_msg().pop_front();
        }
    }
}

