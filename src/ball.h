#ifndef PONG_BALL
#define PONG_BALL
#include "object.h"

class Ball : public Object {
    friend class Paddle;
    friend class PongGame;
public:
    // Default constructor, (x coord, y coord, radius).
    Ball(double, double, double);
    // Stops the ball.
    void stop();

    // Gets the coordinates offset to the top left instead of center.
    double get_top_left_x();
    double get_top_left_y();

    // Getters.
    double get_w();
    double get_h();

private:
    double r_;
};
#endif
