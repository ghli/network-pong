#include "ball.h"

                                            // 0, 0, default velocity.
Ball::Ball(double x, double y, double r) : Object(x,y,0,0), r_(r) {}

void Ball::stop() {
    // Zeros the velocity.
    v_x_ = 0;
    v_y_ = 0;
}

double Ball::get_top_left_x() {
    // our _x is at the center, but we want top left,
    // Which would be _x offset by half of r.
    return x_ - r_/2;
}

double Ball::get_top_left_y() {
    // our _y is at the center, but we want top left,
    // Which would be _y offset by half of r.
    return y_ - r_/2;
}

double Ball::get_w() {
    return r_;
}

double Ball::get_h() {
    return r_;
}
