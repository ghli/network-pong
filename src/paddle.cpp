#include "paddle.h"
#include <boost/geometry/geometries/segment.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include <boost/geometry/algorithms/intersection.hpp>
#include <cmath>
#include "constants.h"

Paddle::Paddle(double x, double y , double w, double h) : Object(x, y, 0, 0),
                                                          w_(w),
                                                          h_(h)
                                                          {}

namespace bg = boost::geometry;
typedef bg::model::point<double, 2, bg::cs::cartesian> Point;
typedef bg::model::segment<Point> Segment;

bool Paddle::handle_collision(Ball& b) {
    double relative_y_before = b.y_ - y_;
    double relative_y_after = b.y_ + 2 * b.v_y_ - y_;

    // If we don't cross the paddle's y coordinate, we're fine.
    // The relative y's signs will differ if we cross.
    if (copysign(1, relative_y_before) == copysign(1, relative_y_after))
        return false;

    Segment paddle_surface(Point(x_ - w_/2, y_),
                           Point(x_ + w_/2, y_)); 

    Segment ball_path(Point(b.x_ - kArrowLen * b.v_x_, b.y_ - kArrowLen * b.v_y_),
                      Point(b.x_ + kArrowLen * b.v_x_, b.y_ + kArrowLen * b.v_y_));

    // Successfull bounce.
    // The ball's path will cross the paddle surface in this case.
    if (bg::intersects(paddle_surface, ball_path)) {
        // If we shoot a line from the center of the paddle to the ball,
        // That will be the new direction of the ball's velocity.

        // Get the direction vector between paddle and ball.
        double x_direction = b.x_ - x_;
        // Scale y towards center so the angle isn't ridiculous.
        double y_direction = (b.y_ - 10*b.v_y_) - y_;
        double old_v_mag = b.v_magnitude();

        // Normalize vectors, so we can directly scale by |v|.
        double magnitude = sqrt(pow(x_direction, 2) + pow(y_direction, 2));
        x_direction /= magnitude;
        y_direction /= magnitude;

        // Set new velocity in specified direction.
        b.v_x_ = old_v_mag * x_direction;
        b.v_y_ = old_v_mag * y_direction;
        return false;
    }

    // Unsuccessful interception :^(.
    return true;
}

double Paddle::get_top_left_x() {
    return x_ - w_/2;
}
double Paddle::get_top_left_y() {
    return y_ - h_/2;
}

double Paddle::get_w() {
    return w_;
}

double Paddle::get_h() {
    return h_;
}

int Paddle::get_score() {
    return score_;
}

void Paddle::set_score(int score) {
    score_ = score;
}
