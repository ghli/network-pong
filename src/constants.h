#ifndef PONG_GAME_CONSTS
#define PONG_GAME_CONSTS
#include <string>

extern const int kGraphicalRotation;
extern const int kArrowLen;
extern const int kMsgExpirationTime;
extern const int kPasswordLen;

extern const double kSpeedScaling;
extern const double kSlowSpeed;
extern const double kDefaultX;
extern const double kDefaultH;
extern const double kDefaultW;
extern const double kDefaultThickness;
extern const double kDefaultPaddleWidth;

extern const std::string kAddressMsg;
extern const std::string kPortMsg;
extern const std::string kPasswordMsg;
extern const std::string kBlankStr;
extern const std::string kDefaultAddr;
extern const std::string kDefaultPort;
extern const std::string kOpponentQuitMsg;
extern const std::string kDefaultPass;

#endif
