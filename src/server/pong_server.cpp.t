#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <pthread.h>
#include <argp.h>
#include <vector>
#include <map>
#include <errno.h>
#include <pthread.h>
#include "game_info.h"
#define PASSWORD_LEN 20

// ARGP structs:
struct argp_option options[] = {
    {"port",    'p',    "PORT",  0,  "Port number of server"},
};

static char doc[] = "A simple server for the little chat client I made.";

static char args_doc[] = "ARG1 ARG2 ARG3";

struct arguments {
    unsigned short port;
};
// End structs.

// ARGP option parser.
static error_t parse_opt (int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = (struct arguments*)state->input;
    switch(key) {
        case 'p': // Port specification.
            sscanf(arg, "%hi", &(arguments->port));
        break;
        case ARGP_KEY_END: // If they've provided too many arguments.
            if (state->arg_num > 1) {
                printf("Too many arguments :^\\\n");
                exit(0);
            }
    }
    return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc };

std::map<std::string, int> waitlist;

// Input struct for thread.
struct Players {
    int player_one;
    int player_two;
};

void* server_instance(void *input) {
    // Get the players.
    int player_one = ((Players*)input)->player_one;
    int player_two = ((Players*)input)->player_two;

    GameInfo info;

    // Tell player one he's player one.
    memset(&info, 0, sizeof(info));
    strncpy(info.control_msg, "1", 2);
    send(player_one, &info, sizeof(info), 0);

    // Tell player two he's second.
    memset(&info, 0, sizeof(info));
    strncpy(info.control_msg, "2", 2);
    send(player_two, &info, sizeof(info), 0);

    while (1) {
        // This pattern ensures that horrible lag goes both ways.
        // Player one sends, player two recieves.
        memset(&info, 0, sizeof(info));
        if (recv(player_one, &info, sizeof(info), MSG_WAITALL) <= 0) {
            // If the connection is closed by player one,
	    // The function recv sets errno to ECONNRESET.
            if (errno == ECONNRESET) {
                strncpy(info.control_msg, "D", 2);
                send(player_two, &info, sizeof(info), 0);
                return NULL;
            }
        }
	// We do MSG_NOSIGNAL because the default behaviour for a failed send,
	// Is a POSIX SIGPIPE, which terminates the program,
	// But we want to handle the failed send first.
        if (send(player_two, &info, sizeof(info), MSG_NOSIGNAL) <= 0) {
	    // If the send failed, it means player two disconnected,
	    // In which case, send sets errno to EPIPE.
	    // We'll have to notify player one something's wrong.
	    if (errno == EPIPE) {
		strncpy(info.control_msg, "D", 2);
                send(player_one, &info, sizeof(info), 0);
                return NULL;
	    }
	};

        // Other way around.
	// This is a mirror of the above code,
	// With player_one and player_two switched.
        memset(&info, 0, sizeof(info));
        if (recv(player_two, &info, sizeof(info), MSG_WAITALL) <= 0) {
            if (errno == ECONNRESET) {
                strncpy(info.control_msg, "D", 2);
                send(player_one, &info, sizeof(info), 0);
                return NULL;
            }
        }
        if (send(player_one, &info, sizeof(info), MSG_NOSIGNAL) <= 0) {
	    if (errno == EPIPE) {
		strncpy(info.control_msg, "D", 2);
                send(player_two, &info, sizeof(info), 0);
                return NULL;
	    }
	}
    }
}

int main(int argc, char **argv) {
    // Argument handling.
    struct arguments args;
    args.port = 5000;
    argp_parse(&argp, argc, argv, 0, 0, &args);

    // File descriptors of the listening socket and connection socket.
    int listener_desc = 0;

    // Get a socket for the listener.
    listener_desc = socket(AF_INET,SOCK_STREAM, 0);
    // FDs should never be < 0.
    if (listener_desc < 0) {
        printf("Failed to acquire socket.\n");
        return -1;
    }

    // Local port setup.
    // Zero our serv_addr struct.
    struct sockaddr_in serv_addr;
    memset(&serv_addr, '0', sizeof(serv_addr));
    // We want a (I)nter(NET) connection (tcp).
    serv_addr.sin_family = AF_INET;
    // We accept any connections.
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    // We want to bind on the port specified.
    serv_addr.sin_port = htons(args.port);

    // We bind our listener socket to the specified port.
    bind (listener_desc, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

    // Set up our listener to queue up to 10 connections.
    // The function listen returns -1 on errors.
    if(listen(listener_desc, 2) == -1) {
        printf("Couldn't listen on port %d", args.port);
        return -1;
    }

    char pass[PASSWORD_LEN]={'\0'};
    std::vector<pthread_t> threads;
    while (true) {
        // Accept a client.
        int client = accept(listener_desc, (struct sockaddr*)NULL, NULL);
        // Zero the password before receving.
        memset(pass, 0, sizeof(pass));
        recv(client, pass, sizeof(pass), MSG_WAITALL);
        // If we've set up the connection properly, the file discriptor should,
        // Be above 0.
        if (client > 0) {
            // If we don't have a player using the password already.
            if (waitlist.find(std::string(pass)) == waitlist.end()) {
                // Add the client to the waitlist.
                waitlist[std::string(pass)] = client;
            // If we have a player using the password recieved.
            } else {
                // Add a new identifier for the thread.
                threads.push_back(0);
                // Create a new input struct for the threaded function.
                Players players;
                // Set the inputs
                players.player_one = waitlist.find(std::string(pass))->second;
                players.player_two = client;
                // Remove the player from the waitlist.
                waitlist.erase(std::string(pass));
                // Create the thread.
                // It exits when a player quits.
                pthread_create(&threads.back(), NULL, &server_instance, &players);
            }
        }
    }

}
