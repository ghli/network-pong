#include "constants.h"

const int kGraphicalRotation = 40;
const int kArrowLen = 50;
const int kMsgExpirationTime = 5;
const int kPasswordLen = 20;

const double kSpeedScaling = 100.0;
const double kSlowSpeed = 5.0;
const double kDefaultX = 100;
const double kDefaultH = 300;
const double kDefaultW = 200;
const double kDefaultThickness = 10;
const double kDefaultPaddleWidth = 100;

const std::string kAddressMsg("Server address:");
const std::string kPortMsg("Server port:");
const std::string kPasswordMsg("Password:");
const std::string kBlankStr("");
const std::string kDefaultAddr("127.0.0.1");
const std::string kDefaultPort("5000");
const std::string kOpponentQuitMsg("Your opponent quit!");
const std::string kDefaultPass("passwd");

