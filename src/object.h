#ifndef PONG_BASE_OBJ
#define PONG_BASE_OBJ
#include <utility>
// Foreward declarations, because Object is the end of the inclusion tree :^\.
// We don't want cyclic inclusions, after all.
class Ball;
class Paddle;
class PongGame;

class Object {
    friend class Ball;
    friend class Paddle;
    friend class PongGame;
public:
    // Constructor. (x coord, y coord, x velocity, y velocity).
    Object(double, double, double, double);

    // Move based on velocity.
    void move();
    // Move to specific cooridinate.
    void move(double, double);
    // Apply friction to the velocity.
    void apply_friction();

    // Get magnitude of velocity.
    double v_magnitude();

    // Getters.
    double get_x();
    double get_y();
    double get_vx();
    double get_vy();
    // Abstract objects have no dimensions.
    virtual double get_w() = 0;
    virtual double get_h() = 0;
    double get_friction();

    // Gets coordinates relative to top left and not center of object.
    virtual double get_top_left_x() = 0;
    virtual double get_top_left_y() = 0;
private:
    double x_;
    double y_;
    double v_x_;
    double v_y_;
    double friction_;
};
#endif
