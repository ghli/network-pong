#include "ofMain.h"
#include "ofApp.h"
#define DISPLAY_MODE OF_WINDOW
int main() {
    // Set up the window.
    ofSetupOpenGL(1920, 1080, DISPLAY_MODE);
    // 60 fps pc masterrace.
    ofSetFrameRate(60);

    Ponger* ponger = new Ponger();
    ponger->set_params(1280, 720);
    ofRunApp(ponger);
}
