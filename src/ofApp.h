#pragma once
#include "ofMain.h"
#include "pong_client.h"
#include "ofxGui.h"

class Ponger : public ofBaseApp {
    public:
        // Inherited methods from ofBaseApp.
        void setup();
        void update();
        void draw();

        // Helper method for draw.
        void draw_game_objects(double, double);

        // Set up our Ponger game.
        void set_params(double, double);

        // More inherited methods from ofBaseApp.
        // Movement from keys.
        void keyPressed(int);
        // Stopping said movement.
        void keyReleased(int);
        // Movement from mouse.
        void mouseMoved(int, int);

        // Button actions.
        void mode_toggle_action();
        void address_entry_action();
        void port_entry_action();
        void password_entry_action();
        void game_start_action();

        // Checks for expired opponent messages and deletes them.
        void expire_messages();
    private:
        PongClient pg_;

        ofTrueTypeFont score_text_;
        ofTrueTypeFont chat_text_;
        ofSoundPlayer bouncy_;

        ofxPanel menu_;
        ofxButton mode_toggle_;
        ofxLabel mode_label_;
        ofxButton address_entry_;
        ofxLabel address_label_;
        ofxButton port_entry_;
        ofxLabel port_label_;
        ofxButton password_entry_;
        ofxLabel password_label_;
        ofxButton start_button_;

        bool started_;

        std::string address_;
        std::string port_;
        std::string password_;
        std::string msg_buf_;

        time_t last_msg_clear_;
};
