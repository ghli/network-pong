#ifndef PONG_CLIENT
#define PONG_CLIENT
#include "pong_game.h"
#include <string>
#include <deque>

// Wraps a pong game.
class PongClient : public PongGame {
public:
    // Initialize client with server address and port.
    std::string init(std::string, std::string, std::string);
    // Update 1 tick.
    void update();

    // Getters.
    bool is_local();
    bool early_exit(); 

    // Setters.
    void set_local(bool);

    // Send a message to the opponent.
    void send_msg(std::string);

    // Get the messages the opponent sent.
    std::deque<std::string>& get_msg();

private:
    GameInfo info_;
    bool local_game_;
    bool local_simulation_;
    bool opponent_quit_ = false;
    int connection_fd_;
    
    void send_data();
    void recieve_data();

    std::deque<std::string> opponent_msg_;
    std::deque<std::string> outgoing_msg_;

};
#endif
