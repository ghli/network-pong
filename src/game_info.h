#ifndef PONG_GAME_INFO_STRUCT
#define PONG_GAME_INFO_STRUCT
#define CNTRL_MSG_SIZE 2
#define CHAT_CHAR_LMT 140
// Struct that holds information vital for cross client play.
struct GameInfo {
    char control_msg[CNTRL_MSG_SIZE];
    // Extra char for null terminator, never 5get.
    char chat_msg[CHAT_CHAR_LMT + 1];
    int p1_score;
    int p2_score;
    double p1_x;
    double p1_y;
    double p2_x;
    double p2_y;
    double ball_x;
    double ball_y;
    double ball_vx;
    double ball_vy;
};
#endif
