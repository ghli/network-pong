#include "object.h"
#include <cmath>
#include <ctgmath>

Object::Object (double x, double y, double v_x, double v_y): x_(x),
                                                             y_(y),
                                                             v_x_(v_x),
                                                             v_y_(v_y)
                                                             {}
void Object::move() {
    // Just apply velocity.
    x_ += v_x_;
    y_ += v_y_;
}

void Object::move(double x, double y) {
    // Teleport object to coordinates.
    x_ = x;
    y_ = y;
}

void Object::apply_friction() {
    // If our length/time is less than len/time^2, we stop.
    // Normally, this would be an integral, but time is discrete here.
    // every call to apply_friction is one tick of time.
    if (abs(v_x_) >= friction_) {
        v_x_ = 0;
    } else {
        v_x_ -= copysign(friction_, v_x_);
    }

    // Same principle as above, but for y velocity.
    if (abs(v_y_) >= friction_) {
        v_y_ = 0;
    } else {
        v_y_ -= copysign(friction_, v_y_);
    }
}

double Object::v_magnitude() {
    // Pythogorean formula.
    return sqrt(pow(v_x_, 2) + pow(v_y_, 2));
}

double Object::get_x() {
    return x_;
}
double Object::get_y() {
    return y_;
}
double Object::get_vx() {
    return v_x_;
}
double Object::get_vy() {
    return v_y_;
}
double Object::get_w() {
    return 0;
}
double Object::get_h() {
    return 0;
}
// We're an abstract object, so there's no way of knowing "true top left",
// Thus, we return our center coordinate.
double Object::get_top_left_x() {
    return x_;
}

// We're an abstract object, so there's no way of knowing "true top left",
// Thus, we return our center coordinate.
double Object::get_top_left_y() {
    return y_;
}
