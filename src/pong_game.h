#ifndef PONG_GAME
#define PONG_GAME
#include "paddle.h"
#include "ball.h"
#include <string>
#include <utility>
#include "game_info.h"

class PongGame {
public:
    PongGame();
    // Updates 1 tick of the game.
    void update();
    // Detect collisions for whole game.
    void collision_detection();

    // Setters.
    void set_w(double);
    void set_h(double);
    void set_second(bool);

    // Getters.
    double get_w();
    double get_h();

    // Moves player one's paddle.
    void move_one(double, double);
    // Moves player two's paddle.
    void move_two(double, double);
    // Moves the ball.
    void move_ball(double, double);

    // Set cartesian velocity of paddle one.
    void set_v_one(double, double);
    // Set cartesian velocity of paddle two.
    void set_v_two(double, double);
    // Set cartesian velocity of the ball.
    void set_v_ball(double, double);

    // Get the member objects. It's ok, because there are copies.
    Paddle get_one();
    Paddle get_two();
    Ball get_ball();
    // Resets the ball to their initial position.
    void reset();
    // Shoots the ball.
    void shoot_ball();

    // Serialize the game data for sending.
    GameInfo serialize();

    // Deserialize a GameInfo.
    void deserialize(GameInfo&);

    // Did we bounce in the last update?
    bool bounced();

private:
    Paddle one_;
    Paddle two_;
    Ball ball_;
    bool reset_;
    int w_;
    int h_;
    double old_vx_;
    double old_vy_;
    bool second_engine_;

    // Saves old ball V for bounce detection.
    void save_velocity();
};

#endif
