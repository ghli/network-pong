#Development

## Outline/Details to note
* Pong Game Engine and Objects separate from UI and Network code.
* Client 1 simulates 1 frame -> Hands to server -> gives to Client 2 to simulate 1 frame.
* UI code wraps Client code that wraps Game Engine (?).

## 4/19/18
* Created pong game engine and objects.
  * Created an object superclass
  * Created paddle and ball derived classes
  * Created a pong game engine class.
* Imported socket networking code from previous personal project. 
### Problems
* Movement of paddles are unchecked
* There is no GUI yet, but one could be easily coupled.
* Socket networking code looks more like C than C++.
* Collision detection are in engine, which is not the best.

## 4/23/18
* Styled Socket code to Google C++ standards.
* Changed server backend to specialize in a two player pong game.
* Refactored collision code into paddles.
  * This enables us to vary the angle of the returning ball based on where it hit.
* Created openFrameworks graphical wrapper as proof of concept.
### Problems
* Socket code still not integrated with game code.
* Neither sound nor 3D yet.
* Partitioning of responsibilities between server and client not fleshed out.

## 4/24/18 ~3AM
* Implemented rudimentary 3D.
* Two player local pong completely implemented.
### Problems
* No networking yet.
* Still need sound.
* 3D looks pretty rough, tbh.

## 4/24/18 A more reasonable time o'clock
* Commented more.
* Changed bouncing physics for more skill.
### Problems
* Still no networking.
* Still no sound.

## 4/24/18
* Tweaked collision physics.
* Tweaked graphics.
* Created client for networking.
* Went throught the whole Google C++ style guide and followed it.

## 4/26/18
* Added network client code.
* Tweaked server.
### Problems
* Collision detection is wonky with networks, it's probably because it happens every other frame now, which messes up the old collision code, which was dependent on 1 unit of velocity (techinically "quantum" :^)).
* Need to add sound and chat.

## 4/27/18, early morning
* Cleaned up code.

## 4/30/18
* Added menu.
* Added sound (finally).
### Problems
* NONE!

## 5/1//18, early morning ~3am
* Polished server code.
### Problems
* openFrameworks is seriously annoying if there's multiple main. The server and client obviously shouldn't be in one binary, and thus should have separate mains, but openframeworks doesn't provide an option to exclude a file from linkage.
* A server execution is one use, fix is probably one line.

## 5/2/18
* Polished code.
* Added chat!
* Added password based connection.
### Problems
* No obvious problems.
* Fixed aforementioned problems.
